## EmojiCon 

EmojiCon is a DevOps conference where all your slides are emoji run by Emojictl - a company interested in teaching tech concepts in fun, interesting ways. (Note reader - [this is a twitter joke.](https://twitter.com/garethgreenaway/status/1574863559623786496?s=20&t=RceIa5_Y0A-C8XJDwigClA))

Built on the 🚀 [AstroWind theme](https://astrowind.vercel.app/) for [Astro](https://astro.build/) - an all-in-one web framework for building fast, content-focused websites.

**EmojiCon** is licensed under the MIT license — see the [LICENSE](https://github.com/onwidget/astrowind/blob/main/LICENSE.md) file for details.

