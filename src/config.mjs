export const SITE = {
	name: 'EmojiCon',

	origin: 'https://emojicon.live',
	basePathname: '/',

	title: 'EmojiCon 🪄💻✨',
	description: 'Emojicon 🪄💻✨ is a DevOps conference where your slides are all emojis run by the Emojictl Collective.',

};

export const BLOG = {
	disabled: false,
	postsPerPage: 4,

	blog: {
		disabled: false,
		pathname: 'blog', // blog main path, you can change this to "articles" (/articles)
	},

	post: {
		disabled: false,
		pathname: '', // empty for /some-post, value for /pathname/some-post 
	},

	category: {
		disabled: false,
		pathname: 'category', // set empty to change from /category/some-category to /some-category
	},

	tag: {
		disabled: false,
		pathname: 'tag', // set empty to change from /tag/some-tag to /some-tag
	},
};
